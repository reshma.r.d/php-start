<?php

class Db{

  function __construct(){}

  public static function connect(){
    $servername = "localhost";
    $username = "root";
    $password = "root";
    $dbname = "test_db";
    $conn = new mysqli($servername, $username, $password, $dbname);
    //  Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    //echo "Connected successfully!..";
    return $conn;
  }
}
?>
