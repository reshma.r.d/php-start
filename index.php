

<html>
<head>
<style>
</style>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title><b> test</b> </title>
</head>
  <body>
  <form method ="post" action="navigate.php">
  <div class="jumbotron jumbotron-fluid text-center">
    <h1>
      <i class="fa fa-user-circle" aria-hidden="true"></i>
      Enter User details</h1>

  </div>
  <div  class="d-flex justify-content-around">
    <a href="listusers.php" id="myBtn">
      <i class="fa fa-user-circle" aria-hidden="true"></i>
        <span>List Users</span>
      </i>
    </a>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-sm-4 form-group">
        <label>First Name: </label>
        <input type="text" class="form-control"  name = "fname" required/><br/>
      </div>
      <div class="col-sm-4 form-group">
        <label>Last Name: </label>
        <input type="text" class="form-control" name = "lname" required/><br/>
      </div>
      <div class="col-sm-4 form-group">
        <label>Email: </label>
        <input type="text" class="form-control" name = "email" required/><br/>
      </div>


    </div>
    <div class ="row">
      <div class="col-sm-4 form-group">
        <label>Mobile: </label>
        <input type="number" class="form-control"  name = "mobile" required maxlength="10"/><br/>
      </div>
      <div class="col-sm-8 form-group">
          <label>Address: </label>
          <textarea name = "address" class="form-control" maxlength="100" required></textarea><br/>
      </div>
    </div>
    <div class ="row">
      <div class="col-sm-6">
          <div class="text-right">
        <input type="submit" name ="submitButton"  class="btn btn-primary">
        <button type="reset" value="Reset" class="btn btn-secondary">Cancel</button>
      </div>
      </div>
    </div>
</div>

</form>
</body>

</html>
