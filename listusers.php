<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
  <form method ="post" action="navigate.php">
<div class="container">
  <h2>User Details</h2>
  <div id="norecord" ></div>
  <table class="table table-condensed tableClass">
    <thead>
      <tr class="active">
        <th><b>First Name</b></th>
        <th><b>Last Name</b></th>
        <th><b>Email</b></th>
        <th><b>Mobile Number</b></th>
        <th><b>Address</b></th>
        <th></th>
      </tr>
    </thead>
    <tbody class="listusersClass">
    </tbody>
  </table>
</div>

<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" >
          <h4>Edit user with id:<span id="user_id"></span>
          </h4>
           <input type="hidden" name="user_name" id="userid" value="" />
          <button type="button" class="close" data-dismiss="modal">&times;</button>

        </div>
        <div class="modal-body" >
          <div class="container">
            <div class="row">
              <div class="col-sm-3 form-group">
                <label>First Name: </label>
                <input type="text" class="form-control"  name="fname_name" id = "fname_id"/><br/>
              </div>
              <div class="col-sm-3 form-group">
                <label>Last Name: </label>
                <input type="text" class="form-control"  name="lname_name" id = "lname_id"/><br/>
              </div>
            </div>
            <div class ="row">
              <div class="col-sm-3 form-group">
                <label>Email: </label>
                <input type="text" class="form-control"  name="email_name" id = "email_id"/><br/>
              </div>
              <div class="col-sm-3 form-group">
                <label>Mobile: </label>
                <input type="number" class="form-control"  name="mobile_name" id = "mobile_id"/><br/>
              </div>
            </div>
              <div class ="row">
              <div class="col-sm-3 form-group">
                  <label>Address: </label>
                  <textarea id = "address_id" class="form-control" name="address_name"></textarea><br/>
              </div>
            </div>
        </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" name ="updateUser">
            <span class="glyphicon glyphicon-pencil"></span> update</button>
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> Cancel</button>
      </div>

    </div>
  </div>
</div>
</form>
</body>
<script>
$(document).ready(function(){
  $.ajax({
        url: "navigate.php",
        type: "post",
        data: {"getuser":"tr"},
        dataType: "json",
        success: function (response) {
          var data = "";
          console.log(response);
          $.each(response,function(i,ele){
            data = data + "<tr id="+ele.id+"_row><td class= 'fname_'>"+ele.first_name+"</td>"+
            "<td class= 'lname_'>"+ele.last_name+"</td><td class= 'email_'>"+ele.email+"</td><td class='mobile_'>"
            +ele.mobile+ "</td><td class='address_'>"+ele.address+"</td><td>"+
            "<i id="+ele.id+" class='fa fa-edit btn btn-link' style='width:10%' onclick='update(this.id)'></i>"+
            "<button class='btn btn-link fa fa-trash' type='submit' name ='deleteUser' style='width:10%' value='"+ele.id+"'> </button></td></tr>";
          });
           $("#norecord").html('');
            $('.listusersClass').html(data);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           $('.tableClass').html('');
           $("#norecord").html("<h3 class='jumbotron jumbotron-fluid text-center'>No records found</h3>");

        }
    });

});

function update(id){
  $('#'+id+'_row').each(function(){
      $('#user_id').html(id);
      $('#userid').val(id);
   $(this).find('td').each(function(){
        if($(this).attr('class') != undefined){
        var id = $(this).attr('class')+"id";
        $('#'+id).val(this.innerHTML);
     }
   })
})
   $("#myModal").modal();
}

</script>

</html>
