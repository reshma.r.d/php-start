
<?php
include("db.php");
class User {

  private $id;
  private $firstname;
  private $lastname;
  private $email;
  private $mobile;
  private $address;

    function __construct($id,$firstname,$lastname, $email,$mobile, $address){
      $this->id = $id;
      $this->firstname = $firstname;
      $this->lastname = $lastname;
      $this->email = $email;
      $this->mobile = $mobile;
      $this->address = $address;

     }

     function insertUserData(){

       $sql = "INSERT INTO test_user (first_name,last_name,email, mobile, address)
       VALUES ('".$this->firstname."','".$this->lastname."','".$this->email."','".$this->mobile."','".$this->address."')";
       $conn = Db::connect();
       if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
            header('Location: listusers.php');
        } else {
            echo "Error: " . $sql . $conn->error;
            header('Location: error.php');
        }
      $conn->close();
     }

     public static function listUser(){
       $sql = "select id,first_name,last_name,email, mobile, address from test_user";
       $conn = Db::connect();
       $result = $conn->query($sql);
      if ($result->num_rows > 0) {
           $outp = $result->fetch_all(MYSQLI_ASSOC);
           echo json_encode($outp);
       } else {
          echo "Error: " . $sql . $conn->error;
          header('Location: error.php');
       }
      $conn->close();
     }

     function updateUser(){
       $conn = Db::connect();
       $sql = "UPDATE test_user SET first_name='".$this->firstname."',last_name='".$this->lastname."',
       email='".$this->email."', mobile='".$this->mobile."',
                address='".$this->address."' WHERE id='".$this->id."' ";
       if ($conn->query($sql) === TRUE) {
           echo "Record updated successfully";
           header('Location: listusers.php');
       } else {
           echo "Error updating record: " . $sql . $conn->error;
             header('Location: error.php');
       }
       $conn->close();
     }

     function deleteUser(){
       $conn = Db::connect();
       $sql = "DELETE FROM test_user WHERE id= '".$this->id."' ";
        if ($conn->query($sql) === TRUE) {
            echo "Record deleted successfully";
            header('Location: listusers.php');
        } else {
            echo "Error deleting record: " . $sql . $conn->error;
            header('Location: error.php');
        }
       $conn->close();
     }

}

?>
